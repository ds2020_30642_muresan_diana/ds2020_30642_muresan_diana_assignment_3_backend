package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;
    private final PatientService patientService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService, PatientService patientService) {
        this.caregiverService = caregiverService;
        this.patientService = patientService;
    }



    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);

    }


    @PostMapping()
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDetailsDTO caregiverDTO) {
        UUID caregiverID = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDetailsDTO> getCaregiver(@PathVariable("id") UUID caregiverId) {
        CaregiverDetailsDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/get-by-name/{name}")
    public ResponseEntity<CaregiverDetailsDTO> getCaregiverByName(@PathVariable("name") String caregiverName) {
        CaregiverDetailsDTO dto = caregiverService.getCaregiverByName(caregiverName);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/get-by-username/{username}")
    public ResponseEntity<CaregiverDetailsDTO> getCaregiverByUsername(@PathVariable("username") String caregiverUsername) {
        CaregiverDetailsDTO dto = caregiverService.getCaregiverByUsername(caregiverUsername);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteCaregiver(@PathVariable("id") UUID caregiverId) {
        caregiverService.deleteCaregiverById(caregiverId);
        return new ResponseEntity<>(caregiverId, HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<CaregiverDetailsDTO> updateCaregiver(@Valid @RequestBody CaregiverDetailsDTO caregiverDTO) {
        caregiverService.updateCaregiverById(caregiverDTO);
        return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }

}
