package ro.tuc.ds2020.entities;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="Activity")
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID activityId;

    @Column(name = "patientId", nullable = false)
    private UUID patientId;

    @Column(name = "activity", nullable = false)
    private String activity;

    @Column(name = "startTime", nullable = false)
    private Date startTime;

    @Column(name = "endTime", nullable = false)
    private Date endTime;

    public Activity() {

    }

    public Activity(UUID id, UUID patientId, String activity, Long start, Long end) {
        this.activityId = id;
        this.patientId = patientId;
        this.activity = activity;
        this.startTime =  new Date(start);
        this.endTime = new Date(end);
    }

    public Activity(UUID patientId, String activity, Long start, Long end) {
        this.patientId = patientId;
        this.activity = activity;
        this.startTime =  new Date(start);
        this.endTime = new Date(end);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return activityId;
    }

    public void setId(UUID id) {
        this.activityId = id;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }


    public Long getStart() {
        return startTime.getTime();
    }

    public void setStart(Long start) {
        this.startTime = new Date(start);
    }

    public Long getEnd() {
        return endTime.getTime();
    }

    public void setEnd(Long end) {
        this.endTime = new Date(end);
    }



}
