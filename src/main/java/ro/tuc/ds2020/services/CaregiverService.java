package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, AccountRepository accountRepository) {
        this.caregiverRepository = caregiverRepository;
        this.accountRepository = accountRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(CaregiverDetailsDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toCaregiver(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public CaregiverDetailsDTO findCaregiverById(UUID id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDetailsDTO(caregiverOptional.get());
    }

    public CaregiverDetailsDTO getCaregiverByName(String name) {

        Optional<Caregiver> caregiverOptional = caregiverRepository.getCaregiverByName(name);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with name {} was not found in db", name);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with name: " + name);
        }
        return CaregiverBuilder.toCaregiverDetailsDTO(caregiverOptional.get());
    }

    public CaregiverDetailsDTO getCaregiverByUsername(String username) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.getCaregiverByUsername(username);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with username {} was not found in db", username);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with name: " + username);
        }
        return CaregiverBuilder.toCaregiverDetailsDTO(caregiverOptional.get());
    }

    public void deleteCaregiverById(UUID caregiverId) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(caregiverId);
        Caregiver caregiver = caregiverOptional.get();
        Optional<Account> accountOptional = accountRepository.getAccountByUsername(caregiver.getUsername());
        Account account = accountOptional.get();
        accountRepository.deleteById(account.getId());
        caregiverRepository.deleteById(caregiverId);
    }

    public CaregiverDetailsDTO updateCaregiverById(CaregiverDetailsDTO caregiverDTO) {
        List<Account> accounts = accountRepository.findAll();
        Caregiver caregiver = CaregiverBuilder.toCaregiver(caregiverDTO);
        Optional<Caregiver> oldCaregiverOptional = caregiverRepository.findById(caregiver.getId());
        Caregiver oldCaregiver = oldCaregiverOptional.get();
        Account account = null;
        for (Account acc : accounts) {
            if (acc.getUsername().equals(oldCaregiver.getUsername())) {
                account = acc;
                break;
            }
        }
        account.setUsername(caregiver.getUsername());
        account.setPassword(caregiver.getPassword());
        accountRepository.save(account);
        oldCaregiver.setName(caregiver.getName());
        oldCaregiver.setAddress(caregiver.getAddress());
        oldCaregiver.setBirthdate(caregiver.getBirthdate());
        oldCaregiver.setGender(caregiver.getGender());
        oldCaregiver.setUsername(caregiver.getUsername());
        oldCaregiver.setPassword(caregiver.getPassword());
        Caregiver newCaregiver = caregiverRepository.save(oldCaregiver);
        return CaregiverBuilder.toCaregiverDetailsDTO(newCaregiver);

    }
}
