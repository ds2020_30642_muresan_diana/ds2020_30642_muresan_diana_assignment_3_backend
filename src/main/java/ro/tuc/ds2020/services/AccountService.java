package ro.tuc.ds2020.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.AccountRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<AccountDTO> findAccounts() {
        List<Account> accountList = accountRepository.findAll();
        return accountList.stream()
                .map(AccountBuilder::toAccountDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(AccountDetailsDTO accountDTO) {
        Account account = AccountBuilder.toAccount(accountDTO);
        account = accountRepository.save(account);
        LOGGER.debug("User with id {} was inserted in db", account.getId());
        return account.getId();
    }

    public AccountDetailsDTO updateAccountById(AccountDetailsDTO accountDTO) {

        Account account = AccountBuilder.toAccount(accountDTO);
        Optional<Account> oldAccountOptional = accountRepository.findById(account.getId());
        Account oldAccount = oldAccountOptional.get();
        oldAccount.setUsername(account.getUsername());
        oldAccount.setPassword(account.getPassword());
        oldAccount.setRole(account.getRole());
        Account newAccount = accountRepository.save(oldAccount);
        return AccountBuilder.toAccountDetailsDTO(newAccount);

    }

    public AccountDetailsDTO getAccountByUsername(String username) {
        Optional<Account> accountOptional = accountRepository.getAccountByUsername(username);
        if (!accountOptional.isPresent()) {
            LOGGER.error("Account with username {} was not found in db", username);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with username: " + username);
        }
        return AccountBuilder.toAccountDetailsDTO(accountOptional.get());
    }

}
