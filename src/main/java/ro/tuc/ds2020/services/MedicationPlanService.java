package ro.tuc.ds2020.services;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, PatientRepository patientRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.patientRepository = patientRepository;
    }

    public List<MedicationPlanDTO> findMedicationPlans() {
        List<MedicationPlan> medicationList = medicationPlanRepository.findAll();
        return medicationList.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(MedicationPlanDetailsDTO medicationDTO, UUID patientId) {

        MedicationPlan medicationPlan = MedicationPlanBuilder.toMedicationPlan(medicationDTO);
        medicationPlan = medicationPlanRepository.save(medicationPlan);

        Optional<Patient> patientOptional = patientRepository.findById(patientId);
        Patient patient = patientOptional.get();
        Set<MedicationPlan> medicationPlans = patient.getMedicationPlans();
        medicationPlans.add(medicationPlan);
        patient.setMedicationPlans(medicationPlans);

        patientRepository.save(patient);
        LOGGER.debug("MedicationPlan with id {} was inserted in db", medicationPlan.getId());
        System.out.println("id medicaiton plan" + medicationPlan.getId() + " id pacient" + patientId);
        return medicationPlan.getId();
    }

    public List<MedicationPlanDTO> findMedicationPlansByPatient(UUID patientId) {
        List<Patient> patientList = patientRepository.findAll();
        Set<MedicationPlan> medicationPlan;
        for (Patient p : patientList) {
            if (p.getId().equals(patientId)) {
                medicationPlan = p.getMedicationPlans();
                return medicationPlan.stream()
                        .map(MedicationPlanBuilder::toMedicationPlanDTO)
                        .collect(Collectors.toList());
            }

        }
        return null;

    }

    public UUID updateMedicationPlan(UUID medicationPlanId, UUID patientId, MedicationDetailsDTO medication) {
        Optional<Patient> patientOptional = patientRepository.findById(patientId);
        Patient patient  = patientOptional.get();
        Set<MedicationPlan> medicationPlans = patient.getMedicationPlans();

        for (MedicationPlan mp : medicationPlans) {
            if (mp.getId().equals(medicationPlanId)) {
                Set<Medication> medications = mp.getMedications();
                medications.add(MedicationBuilder.toMedication(medication));
                mp.setMedications(medications);
                break;
            }
        }
        patient.setMedicationPlans(medicationPlans);
        patientRepository.save(patient);
        return medicationPlanId;

    }


}
