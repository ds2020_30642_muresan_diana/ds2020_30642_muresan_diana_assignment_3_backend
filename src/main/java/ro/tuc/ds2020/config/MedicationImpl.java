package ro.tuc.ds2020.config;

import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;

import java.util.Date;
import java.util.List;
import java.util.*;

public class MedicationImpl implements Medication {


    private MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationImpl(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @Override
    public ArrayList<String[]> sendMedication(Date medicationPlanDate, UUID patientId) {
        System.out.println("=============server side==============");

        List<MedicationPlanDTO> medPlans = medicationPlanService.findMedicationPlansByPatient(patientId);

        ArrayList<String[]> processedMedPlans = new ArrayList<>();
        for (MedicationPlanDTO medPlan : medPlans) {
            Set<MedicationDTO> meds = medPlan.getMedications();
            String dates[] = medPlan.getPeriodOfTreament().split("-");
            String startDate[] = dates[0].split("\\.");
            String endDate[] = dates[1].split("\\.");

            Date startDay = new Date();
            startDay.setMonth(Integer.parseInt(startDate[1]) - 1);
            startDay.setDate(Integer.parseInt(startDate[0]));
            startDay.setHours(0);
            startDay.setMinutes(0);
            startDay.setSeconds(0);
            System.out.println(startDay);

            Date endDay = new Date();
            endDay.setMonth(Integer.parseInt(endDate[1]) - 1);
            endDay.setDate(Integer.parseInt(endDate[0]));
            endDay.setHours(0);
            endDay.setMinutes(0);
            endDay.setSeconds(0);
            System.out.println(endDay);

            System.out.println(medicationPlanDate);

            if(startDay.before(medicationPlanDate) && endDay.after(medicationPlanDate)) {

                for (MedicationDTO med : meds) {

                    String[] medication = new String[2];
                    medication[0] = med.getName();
                    medication[1] = medPlan.getIntakeIntervals();
                    processedMedPlans.add(medication);
                }

            }
        }
        System.out.println(processedMedPlans.size());
        return processedMedPlans;
    }



    @Override
    public void getMessage(String message) {
        System.out.println("=============server side==============");
        System.out.println(message);
    }
}
