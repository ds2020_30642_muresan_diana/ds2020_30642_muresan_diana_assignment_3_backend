package ro.tuc.ds2020.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.services.MedicationPlanService;

@Configuration
public class HessianConfiguration {
    @Autowired
    private MedicationPlanService medicationPlanService;

    @Bean(name = "/hellohessian")
    RemoteExporter sayHelloServiceHessian() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new MedicationImpl(medicationPlanService));
        exporter.setServiceInterface(Medication.class);
        return exporter;
    }
}
