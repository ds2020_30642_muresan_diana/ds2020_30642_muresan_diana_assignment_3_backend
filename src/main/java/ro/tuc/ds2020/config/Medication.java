package ro.tuc.ds2020.config;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface Medication {
    public ArrayList<String[]> sendMedication(Date medicationPlanDate, UUID patientId);
    public void getMessage(String msg);
}
