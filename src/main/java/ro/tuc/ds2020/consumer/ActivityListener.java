package ro.tuc.ds2020.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.ActivityController;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.builders.ActivityBuilder;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.repositories.ActivityRepository;
import ro.tuc.ds2020.services.ActivityService;

@Service
public class ActivityListener {


    @Autowired
    private ActivityController activityController;
    @Autowired
    private ActivityService activityService;


    @RabbitListener(queues = "assignment_2_queue")
    public void consumeMessage(final ActivityDTO message)   {


        if (message.getActivity().equals("Sleeping"))
             if (message.getEnd() - message.getStart() > 25200000) {
                 System.out.println("received " + message.toString());
                 activityController.sendActivity(message);
                 System.out.println(message.toString());
                  //activityService.insert(message);
             }

        if (message.getActivity().equals("Leaving"))
            if (message.getEnd()- message.getStart() > 18000000) {
                System.out.println("received " + message.toString());
                activityController.sendActivity(message);
                //activityService.insert(message);
            }

        if (message.getActivity().equals("Toileting") || message.getActivity().equals("Showering") || message.getActivity().equals("Grooming"))
            if (message.getEnd()- message.getStart() > 1800000) {
                System.out.println("received " + message.toString());
                activityController.sendActivity(message);
                //activityService.insert(message);
            }
    }
}
