package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> {

    private UUID id;
    private String intakeIntervals;
    private String periodOfTreament;
    private Set<MedicationDTO> medications;




    public MedicationPlanDTO() {

    }

    public MedicationPlanDTO(UUID id, String intakeIntervals, String periodOfTreament, Set<MedicationDTO> medications ) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.periodOfTreament = periodOfTreament;
        this.medications = medications;

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getPeriodOfTreament() {
        return periodOfTreament;
    }

    public void setPeriodOfTreament(String periodOfTreament) {
        this.periodOfTreament = periodOfTreament;
    }

    public Set<MedicationDTO> getMedications() {
        return medications;
    }

    public void setMedications(Set<MedicationDTO> medications) {
        this.medications = medications;
    }


}
