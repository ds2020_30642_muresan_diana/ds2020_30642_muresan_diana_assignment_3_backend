package ro.tuc.ds2020.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;


public class ActivityDTO implements Serializable {
    private UUID activityId;
    private UUID patientId;
    private String activity;
    private Long startTime;
    private Long endTime;

    public ActivityDTO(@JsonProperty("activityId") final UUID activityId,
                       @JsonProperty("patientId") final UUID patientId,
                       @JsonProperty("activity") final String activity,
                       @JsonProperty("start") final Long start,
                       @JsonProperty("end") final Long end) {
        this.activityId = activityId;
        this.patientId = patientId;
        this.activity = activity;
        this.startTime = start;
        this.endTime = end;
    }

    public UUID getActivityId() {
        return activityId;
    }

    public void setActivityId(UUID activityId) {
        this.activityId = activityId;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getStart() {
        return startTime;
    }

    public void setStart(Long start) {
        this.startTime = start;
    }

    public Long getEnd() {
        return endTime;
    }

    public void setEnd(Long end) {
        this.endTime = end;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "activityId=" + activityId +
                ", patientId=" + patientId +
                ", activity='" + activity + '\'' +
                ", start=" + startTime +
                ", end=" + endTime +
                '}';
    }
}
