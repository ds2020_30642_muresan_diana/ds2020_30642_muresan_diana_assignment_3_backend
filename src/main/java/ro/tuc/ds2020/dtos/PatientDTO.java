package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class PatientDTO extends RepresentationModel<PatientDTO> {
    private UUID id;
    private String name;
    private String address;
    private String birthdate;
    private String gender;
    private String medicalRecord;
    private String username;
    private String password;
    private Set<MedicationPlanDTO> medicationPlans;



    public PatientDTO () {}

    public PatientDTO(UUID id, String name, String address, String birthdate, String gender, String medicalRecord, String username, String password,  Set<MedicationPlanDTO> medicationPlans) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
        this.username = username;
        this.password = password;
        this.medicationPlans = medicationPlans;

    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<MedicationPlanDTO> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlanDTO> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }
}
