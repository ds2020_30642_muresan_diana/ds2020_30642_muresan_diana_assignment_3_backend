package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class PatientDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String address;
    @NotNull
    private String birthdate;
    @NotNull
    private String gender;
    private String medicalRecord;
    @NotNull
    private String username;;
    @NotNull
    private String password;
    private Set<MedicationPlanDetailsDTO> medicationPlans;



    public PatientDetailsDTO() {

    }
    public PatientDetailsDTO(@NotNull String name, @NotNull String address, @NotNull String birthdate, @NotNull String gender, String medicalRecord, @NotNull String username, @NotNull String password, Set<MedicationPlanDetailsDTO> medicationPlans) {
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
        this.username = username;
        this.password = password;
        this.medicationPlans = medicationPlans;

    }

    public PatientDetailsDTO(UUID id, @NotNull String name, @NotNull String address, @NotNull String birthdate, @NotNull String gender, String medicalRecord, @NotNull String username, @NotNull String password, Set<MedicationPlanDetailsDTO> medicationPlans ) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
        this.username = username;
        this.password = password;
        this.medicationPlans = medicationPlans;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<MedicationPlanDetailsDTO> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlanDetailsDTO> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }
}
