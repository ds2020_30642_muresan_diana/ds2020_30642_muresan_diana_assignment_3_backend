package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationDetailsDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.stream.Collectors;

public class MedicationPlanBuilder {
    private static MedicationBuilder medicationBuilder = new MedicationBuilder();


    public MedicationPlanBuilder() {

    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getIntakeIntervals(), medicationPlan.getPeriodOfTreatment(), medicationPlan.getMedications().stream().map(medication -> medicationBuilder.toMedicationDTO(medication)).collect(Collectors.toSet()) );

    }

    public static MedicationPlan toMedicationPlan(MedicationPlanDetailsDTO medicationPlanDetailsDTO) {
        return new MedicationPlan(medicationPlanDetailsDTO.getId() , medicationPlanDetailsDTO.getIntakeIntervals(), medicationPlanDetailsDTO.getPeriodOfTreatment(),medicationPlanDetailsDTO.getMedications().stream().map(medication -> medicationBuilder.toMedication(medication)).collect(Collectors.toSet()) );
    }

    public static MedicationPlanDetailsDTO toMedicationPlanDetailsDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDetailsDTO(medicationPlan.getId(), medicationPlan.getIntakeIntervals(), medicationPlan.getPeriodOfTreatment(),medicationPlan.getMedications().stream().map(medication -> medicationBuilder.toMedicationDetailsDTO(medication)).collect(Collectors.toSet()) );
    }

}
