package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Activity;

public class ActivityBuilder {

    public ActivityBuilder() {
    }

    public static ActivityDTO toActivityDTO(Activity activity) {
        return new ActivityDTO(activity.getId(), activity.getPatientId(), activity.getActivity(), activity.getStart(), activity.getEnd());

    }

    public static Activity toActivity(ActivityDTO activityDTO)  {

        return new Activity(activityDTO.getActivityId(), activityDTO.getPatientId(), activityDTO.getActivity(), activityDTO.getStart(), activityDTO.getEnd());

    }


}
